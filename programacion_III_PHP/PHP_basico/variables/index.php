<html lang="es">
  <head>
    <meta charset="UTF-8" />
  </head>

  <body>
    <header>
      <h1>Variables en PHP</h1>
    </header>

    <section>
      <article>
        <?php
          //Variables definidas
          $nombre='Luis';
          $apellido='Fagúndez';
          $edad=23;
          $esHumano=true;
          echo "<p>El nombre es $nombre, el apellido es $apellido, la edad es $edad y la verificación de ser huamno da: $esHumano </p>";
          echo "<br>";
        ?>
      </article>
    </section>

    <footer>
      <p>Soy un pie de página amigable</p>
    </footer>
  </body>
</html>
